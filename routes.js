
var controller = require('./public/js/controller');
var getAllUsers = controller.getAllUsers;
var deleteUser = controller.deleteUser;
var addUpdateUser = controller.addUpdateUser;
var updateUser = controller.updateUser;
module.exports = function(app) {
    app.get('/', function(req, resp) {
        
        resp.sendFile(__dirname + '/index.html');
    });

    //Get the list of users
    app.get('/getUsers', getAllUsers);
    app.delete('/deleteUser/:name', deleteUser);
    app.post('/addUser', addUpdateUser);

    // Edit and update a user
    app.put('/updateUser', updateUser);
}