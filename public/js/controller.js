
var users = [
    {
        name : "SatyendraKumar"
    },
    {
        name : "User One"
    },
    {
        name : "User Two"
    }
];
var isEditing = false;
function getAllUsers(req, res) {
    res.send({
        data : users,
        error : ""
    });
};
var position = '';
function deleteUser(req, res) {
    var len = users.length;
    var userToDelete = req.params.name;
    console.log(userToDelete);
        for(var indx=0; indx <len; indx++ ) {
            var element = users[indx];
            if(element.name == userToDelete) {
                users.splice(indx, 1);
                break;
            }
        };

        res.send("User Removed Successfully");

}
//Update user
function updateUser(req, res) {
    var newData = req.body;
    console.log("New Data - ", newData);
    isEditing = true;    
        var len = users.length;    
        for(var indx=0; indx <len; indx++ ) {
            var element = users[indx];
            if(element.name == newData.oldName) {
                users[indx] = {
                    name : newData.newName
                };
            }
        };
        res.send("User updated!!");
}
function addUpdateUser(req, res) {
    var newUser = req.body;
    console.log("new user", newUser);
    if(isEditing === true && position != '') {
        users[position] = newUser;
    } else {
        users.push(newUser);
    }
    position = '';
    isEditing = false;
    res.send("New user added..");
}

module.exports = {
    getAllUsers : getAllUsers,
    deleteUser : deleteUser,
    addUpdateUser : addUpdateUser,
    updateUser : updateUser
}