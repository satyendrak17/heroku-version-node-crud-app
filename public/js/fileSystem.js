const fs = require('fs');
//[TODO] Sync version of same
//mkdir

// Read file async
function readWriteFile() {
    fs.readFile(__dirname + '/lorem1.txt', 'utf8', function(error, result) {
        if(error){
            console.log("Error- ", error);
            throw new Error("Something went wrong while reading file.");
        } else {
            fs.writeFile(__dirname +'/writeLorem.txt', result, function(err, result) {
               if(err) {
                    console.log("Write Fail "+ err);
               } else {
                   console.log("Write Pass " , result);                   
               }
            });
        }
    });    
}
//readWriteFile();

//Remove a file
function deleteFile() {
    fs.unlink(__dirname + '/writeLorem.txt', function(e, info) {
        console.log("File removed", e, info);
    });
}
//deleteFile();
// Remove directory
function removDir() {
    // Since directory is not empty lets empty directory here
    fs.unlink('../node_crud_app/public/Files/lorem.txt', function(){
        console.log("File removed");
        fs.rmdir('../node_crud_app/public/Files', function(err, data) {
            if(err) {
                console.log("Folder not removed", err, data);            
    
            } else {
                console.log("Folder removed", data);
            }
            
        });
    });    
}

//removDir();


//ReadStream and write stream

function readWriteStreams() {
    var readStreamData = fs.createReadStream(__dirname + '/lorem.txt', 'utf8');
    var writeStream = fs.createWriteStream(__dirname + '/writeStreamData.txt');
    readStreamData.on('data', function(chunk) {
        console.log("chunk received");
        console.log(chunk);

        writeStream.write(chunk, function() {
            console.log("Stream data written");
        });
    });
}
//readWriteStreams();

// using pipe
function usingPipe() {
    var readStreamData = fs.createReadStream(__dirname + '/lorem.txt', 'utf8');
    var writeStream = fs.createWriteStream(__dirname + '/writeStreamData.txt');

    readStreamData.pipe(writeStream);
}

usingPipe();