// Read/Write from/to a buffer
var buff1 = new Buffer(10);

buff1.write("Satyendra kumar", 0,10, 'utf8');

console.log(buff1.toString());

// Array initialization
//array of bytes to copy from
// These methods are deprecated, use Buffer.from([]); instead
var buff2 = new Buffer([12, 13, 14]);

var buff3 = Buffer.from([12,13, 14]);
console.log(buff2);
console.log(buff3);