var events = require('events');
const util = require('util');

var myEventEmitter = new events.EventEmitter();

myEventEmitter.on('speak', function(data) {
    console.log('data', data);
});
myEventEmitter.emit('speak', {message : 'Hello World!'});
//Attach a speak event to each instance of Person
function Person(name) {
    this.name = name;
}

// Inherit event emitter on every instance of Person class
util.inherits(Person, events.EventEmitter);

var krish = new Person('Krish');
var superMan = new Person("Vin Diesel");
var batman = new Person("Batman");
var personArray = [krish, superMan, batman];

personArray.forEach(function(person){
   person.on('speak', function(msg){
        console.log(person.name +" speaks "+ msg);
   });
});
krish.emit('speak', 'I am a super hero');
