// Create an angular app for CRUD operation
var crudApp = angular.module('crudApp', [])
.controller('SampleCntrl', function($scope, $http) {
    $scope.name = "Satyendra Kumar";
    $scope.users = [];
    function getAllUsers() {
        $http.get('/getUsers').then(function(resp) {
            $scope.users = resp.data.data;
        });
    }
    getAllUsers();
    $scope.isEditing = false;

    var position = '';
    $scope.editUser = function(data, index) {
        $scope.oldName = data.name;
        $scope.newUser = data.name;        
        $scope.isEditing = true;
    };
    

    $scope.deleteUser = function(data) {
        $http.delete('/deleteUser/' + data.name).then(function(resp) {
            console.log("User Deleted", resp);
            //load the fresh list of users
            getAllUsers();
        });
    };

    $scope.addUser = function() {
        if($scope.isEditing === true) {
            $http.put('/updateUser', {
            oldName : $scope.oldName,
            newName : $scope.newUser
        }).then(function(respData) {
            console.log("User updated !!!");
            $scope.isEditing = false;
            $scope.newUser = '';
            $scope.oldName = '';
            getAllUsers();
        });
        } else {            
            var newUser = {
                "name" : $scope.newUser
            };
            $http.post('/addUser', newUser).then(function(resp) {
            console.log("Added new user ", resp);
            newUser = {};
            $scope.newUser = "";
            getAllUsers();
        });
        }
    }
});