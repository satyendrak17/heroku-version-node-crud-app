var express = require("express"),
    bodyParser = require('body-parser'),
    app = express(),
    routes = require('./routes');
    
//require('./public/js/eventEmitter');
//require('./public/js/fileSystem');
//require('./public/js/cluster');
//require('./public/js/buffer');
const PORT = process.env.PORT || 3000;


// To access static files
app.use(express.static(__dirname + '/public'));
// Use body parser middleware to parse post/form data
app.use(bodyParser.urlencoded( {
  extended : true
}));
// parse application/json
app.use(bodyParser.json())

// Route using app
routes(app);

// Route using Router
// for every request which has /book in request se this middleware
//var booksRoute = express.Router();
//require('./public/js/Router')(booksRoute);
//app.use('/books', booksRoute);


app.listen(PORT, function() {
    console.log("Listening on port : " + PORT);
});